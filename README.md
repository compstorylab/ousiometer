
# Ousiometer 


## Installation

You can install the latest verion by cloning the repo and running [setup.py](setup.py) script in your terminal

```shell 
git clone --recursive https://gitlab.com/compstorylab/ousiometer.git
cd ousiometer
python setup.py install 
```


### Install Development Version

```shell
git clone --recursive https://gitlab.com/compstorylab/ousiometer.git
cd ousiometer
python setup.py develop
```
