"""
Tests the pipeline in utils_parse.py
"""
import fasttext
from pathlib import Path
from pprint import pprint

import utils_general as g_utils
import utils_parse as p_utils
from storywrangler import get_emojis_parser
from storywrangler import get_ngrams_parser

cwd = Path.cwd()
resources = cwd.parent / 'storywrangler' / 'resources'
config = {
    "model": resources / "lid.176.bin",
    "model_threshold": 0.25,
    "ousiometer_dir": resources / "ousiometer_scores",
    "emoji_parser": resources / "emojis.bin",
    "ngrams_parser": resources / "ngrams.bin",
}

path = resources / 'raw_tweets' / '2021-03-08' / '2021-03-08-00-00.gz'
clf = fasttext.load_model(str(config['model']))
threshold = config['model_threshold']
emoji_parser = get_emojis_parser(config['emoji_parser'])
ngrams_parser = get_ngrams_parser(config['ngrams_parser'])
target_langs = ['en']
target_ngrams = config['ousiometer_dir']

savepth = cwd / 'results'
ousiometer_scores = target_ngrams
model = config['model']

process_in_ram=True

p_utils.collect_ousiometer_ngrams(
    path.parent,
    str(model),
    emoji_parser,
    ngrams_parser,
    savepth,
    threshold,
    target_langs,
    ousiometer_scores,
    case_sensitive=False,
    anchor_regex=None,
    test=True,
    process_in_ram=process_in_ram
)

