"""
Ousiometer
Copyright (c) 2021 The Computational Story Lab.
Licensed under the MIT License;

Gets the frequencies (raw counts) of the words in the ousiometer lexicon in
15-minute resolutions for each day.
"""

import logging
import shutil
import sys
import time
import ujson
from pathlib import Path

from storywrangler import get_emojis_parser
from storywrangler import get_ngrams_parser
from storywrangler import create_anchor_regex

import utils_general as g_utils
from utils_parse import collect_ousiometer_ngrams


def parse_args(args, config):
    parser = g_utils.create_parser(config)

    parser.add_argument(
        '--config',
        default=Path.cwd().parent / 'resources' / 'config.json',
        type=str,
        help='path of the JSON file containing default values'
    )

    parser.add_argument(
        'datadir',
        type=Path,
        help='data directory containing .tsv.gz files of the '
             'raw 15-minute tweets'
    )

    parser.add_argument(
        '--anchor',
        default=None,
        type=Path,
        help='path to a json file of anchor ngrams to subsample tweets'
    )

    parser.add_argument(
        '--score_dir',
        type=Path,
        default=config['score_dir'],
        help='directory containing the score files, each with '
             'filename <lang>_*.tsv'
    )

    parser.add_argument(
        '--lid_threshold',
        default=config['lid_threshold'],
        type=float,
        help='confidence score threshold for the language identifier'
    )

    parser.add_argument(
        '--outdir',
        type=str,
        default=config['outdir'],
        help='directory where the output files will be placed'
    )

    parser.add_argument(
        '--langs',
        default=config['langs'],
        type=str,
        help='a list of languages to parse out'
    )

    parser.add_argument(
        '--model',
        default=config['model'],
        help='absolute Path to FastText pre-trained model'
    )

    parser.add_argument(
        '--emoji_parser',
        default=config['emoji_parser'],
        help='path to the emojis.bin file'
    )

    parser.add_argument(
        '--ngrams_parser',
        default=config['ngrams_parser'],
        help='path to the ngrams.bin file'
    )

    parser.add_argument(
        '--lowercase',
        action='store_true',
        default=config['lowercase'],
        help='converts all tweets to lowercase before getting the ngram counts'
    )

    parser.add_argument(
        '--no-lowercase',
        action='store_false',
        dest='lowercase',
        help='ngram matching becomes case-sensitive'
    )

    parser.add_argument(
        '--process_in_ram',
        action='store_true',
        default=config['process_in_ram'],
        help='combine dataframes for the 15-min chunks in a day before '
             'writing to a .tsv.gz file'
    )

    parser.add_argument(
        '--no-process_in_ram',
        action='store_false',
        dest='process_in_ram',
        help='reads and writes the output for every 15-min chunk'
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    dummy_defaults = {k: None for k in [
        'score_dir',
        'outdir',
        'model',
        'lid_threshold',
        'langs',
        'emoji_parser',
        'ngrams_parser',
        'lowercase',
        'process_in_ram',
        'test',
        'logdir',
        'logfile',
        'logscreen',
        'overwrite',
    ]}

    dummy_args = parse_args(args, dummy_defaults)
    with open(dummy_args.config, 'r') as cfg:
        config = ujson.load(cfg)

    args = parse_args(args, config)

    logdir = Path(args.logdir) / 'word_counts'
    logdir.mkdir(parents=True, exist_ok=True)
    logname = logdir / f'{Path(args.datadir).stem}.log'

    g_utils.log_sysargs(logname, args, script=sys.argv[0],
                        to_file=args.logfile,
                        to_screen=args.logscreen
                        )
    logging.info('Initialized logging...')
    logging.info(args)

    print = logging.info

    path = args.datadir
    model = args.model
    emoji_parser = get_emojis_parser(args.emoji_parser)
    ngrams_parser = get_ngrams_parser(args.ngrams_parser)
    savepth = args.outdir
    threshold = args.lid_threshold
    target_langs = args.langs.split(' ')
    ousiometer_scoredir = args.score_dir

    if args.anchor is not None:
        anchor_regex = create_anchor_regex(args.anchor)
    else:
        anchor_regex = None

    done = collect_ousiometer_ngrams(
        path,
        model,
        emoji_parser,
        ngrams_parser,
        savepth,
        threshold,
        target_langs,
        ousiometer_scoredir,
        case_sensitive=False if args.lowercase else True,
        anchor_regex=anchor_regex,
        test=args.test,
        process_in_ram=args.process_in_ram,
    )

    if not done:
        print('Deleting temporary files... ')
        shutil.rmtree(savepth, ignore_errors=True)
        exit()

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {args.outdir}')
    print('DONE.')


if __name__ == "__main__":
    main()
