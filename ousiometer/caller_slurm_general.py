"""
Submits a job to slurm for any python file.
"""
import argparse
import logging
import os
import subprocess
import sys
import time
from datetime import datetime


def parse_args(args, defaults):
    from datetime import date
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "pyfile",
        type=str,
        default=defaults['pyfile'],
        help="path of python script"
    )
    parser.add_argument(
        "-p", "--partition",
        type=str,
        default=defaults['partition'],
        help="partition"
    )
    parser.add_argument(
        "--time",
        type=str,
        default=defaults['time'],
        help="running time"
    )
    parser.add_argument(
        "--mem_mb",
        type=int,
        default=defaults['mem_mb'],
        help="RAM needed"
    )
    parser.add_argument(
        "--test_slurm",
        action="store_true",
        help="create a test slurm sh file"
    )
    parser.add_argument(
        "--shdir",
        type=str,
        default=defaults['shdir'],
        help="dir for .sh files created"
    )
    parser.add_argument(
        "--sleep",
        type=int,
        default=defaults['sleep'],
        help="time in between submitting jobs"
    )
    parser.add_argument(
        "--begin",
        type=str,
        default=defaults['begin'],
        help="job start time"
    )

    args, extra = parser.parse_known_args()

    return args, extra

def makedir_if_needed(dir):
    if dir is not None:
        if not os.path.exists(dir):
            os.makedirs(dir)
    else:
        pass

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    defaults = {
                    'pyfile': 'parser.py', # change to correct location
                    'partition': 'bluemoon',
                    'time': '02-00',
                    'mem_mb': 8000,
                    ## 'test_slurm': False,
                    'shdir': os.path.join(os.getcwd(), 'slurm_shfiles'),
                    'sleep': 2,
                    'begin': 'now',

                    'datadir': '/gpfs2/scratch/cdanfort/twitter/tweet-troll/zipped-raw',
            }

    args, extra_kwargs_orig = parse_args(args, defaults)
    makedir_if_needed(os.path.join(os.getcwd(), 'slurm_out'))

    pyfile = args.pyfile
    fname = os.path.splitext(os.path.basename(pyfile))[0]
    partition = args.partition
    runtime = args.time
    mem_mb = args.mem_mb
    shdir = args.shdir
    test_slurm = args.test_slurm

    date = datetime.now()
    extra_kwargs = extra_kwargs_orig[:]

    text = """#!/bin/bash
#SBATCH -p {partition}
#SBATCH --time={runtime}
#SBATCH --mem={mem_mb}
#SBATCH --job-name={fname}
#SBATCH --output=./slurm_out/slurm_%x_%j_{date}.out
#SBATCH --begin={begin}""".format(
    partition=partition,
    runtime=runtime,
    mem_mb=mem_mb,
    date=date.strftime('%Y%m%d-%H%M%S'),
    begin=args.begin,
    fname=fname
)

    if test_slurm:
        text += """
#SBATCH --test-only"""

    text += """
srun python {pyfile} {extra_kwargs_str}
""".format(pyfile=pyfile, extra_kwargs_str=' '.join(extra_kwargs))

    sh_file = 'caller_slurm_{fname}_{date}.sh'.format(
        fname=os.path.splitext(fname)[0], date=date.strftime('%Y%m%d-%H%M%S'))
    makedir_if_needed(shdir)
    sh_file = os.path.join(shdir, sh_file)
    with open(sh_file, 'w') as f:
        f.write(text)

    to_run = ['sbatch', sh_file]
    full_run = ' '.join(to_run)

    res = subprocess.run(to_run)
    logging.info('Running:\n {0}'.format(full_run))

    time.sleep(args.sleep)

if __name__ == '__main__':
    main()