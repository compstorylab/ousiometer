"""
Ousiometer
Copyright (c) 2021 The Computational Story Lab.
Licensed under the MIT License;
"""

import gzip
import time
import pandas as pd
import ujson
import traceback
from pathlib import Path
import logging

import fasttext

from storywrangler import Storyon
from storywrangler import parse_activity
from storywrangler import process_activity

import utils_general as g_utils
from pprint import pprint

print = logging.info


def load_ousiometer_words(path, word_col='word', score_cols=None):
    """
    Load ousiometer ngrams and scores into a dictionary

    Parameters
    ----------
    path: path to ousiometer ngrams score file; TSV files must be
        prefixed with `<language_code>_`
    """
    if score_cols is None:
        score_cols = [
                'valence','arousal','dominance',
                'goodness','energy',
                'structure',
                'power','danger',
            ]

    ngrams = {}
    for p in sorted(Path(path).glob('*.tsv')):
        lang = p.stem.split('_')[0]
        words = g_utils.read_tsvgz(
            p,
            gzip=False,
            header=0,
            usecols= [word_col] + score_cols,
        )

        assert words[word_col].isna().sum() == 0

        words.columns = words.columns.str.replace(' ', '_').str.lower()
        words.set_index(word_col, inplace=True)
        ngrams[lang] = {w: Storyon() for w in words.index}
    return ngrams


def process_ousiometer_batch(
    path,
    clf,
    threshold,
    emoji_parser,
    ngrams_parser,
    target_langs,
    target_ngrams,
    case_sensitive,
    anchor_regex,
    test=False,
):
    """
    Process a 15-minute chunk of tweets

    Parameters
    ----------
    path: path to a 15-minute chunk
    clf: path to a fastText model
    threshold: confidence score threshold for the language identifier
    emoji_parser: a compiled regular expression that matches emojis
    ngrams_parser: a compiled regular expression that matches ngrams
    target_langs: a list of languages to parse out
    target_ngrams: path to labmt files
    case_sensitive: a toggle to get case sensitive ngrams
    anchor_regex: a compiled regex to subsample tweets
    test: a  toggle for debugging mode

    Returns
    -------
    ngrams (NgramCounter) with count, count_no_rt
    """
    acts, corrupted = 0, 0
    ngrams = load_ousiometer_words(target_ngrams)

    with gzip.open(path, 'rb') as data:
        try:
            for line in data:
                if test:
                    if acts == 1000:
                        break

                acts += 1
                if line.isspace():
                    # filter out empty tweets
                    corrupted += 1
                    continue

                try:
                    activities = parse_activity(ujson.loads(line))
                except ValueError:
                    corrupted += 1
                    continue

                for activity in activities:
                    if activity is not None:
                        try:
                            ft_lang, pred, ngs = process_activity(
                                activity,
                                clf,
                                threshold,
                                emoji_parser,
                                ngrams_parser,
                                scheme=1,
                                target_langs=target_langs,
                                case_sensitive=case_sensitive,
                                anchor_regex=anchor_regex
                            )

                            if ngs is not None:
                                if test:
                                    # pprint(activity['text'])
                                    # pprint(pred)
                                    # pprint(ngs)
                                    pass
                                for w in ngs:
                                    try:
                                        ngrams[ft_lang][w] += ngs[w]
                                    except KeyError:
                                        pass

                        except Exception as e:
                            print(e)
                            traceback.print_exc()
                            corrupted += 1
                            continue
        except EOFError:
            print('Warning: 15-minute batch is incomplete!')

    print(f'[{acts}] activities | [{corrupted}] corrupted activities')
    return ngrams


def update_ousiometer(path, df, test=False):
    """
    Write batch updates to daily ousiometer files

    Parameters
    ----------
    path: path to save batch
    df: a dataframe of word counts
    """
    if test:
        print(path)
        print(df.head())
        print(df.tail())

    if path.exists():
        old = g_utils.read_tsvgz(path, header=0, index_col=0)

        assert len(old.columns) == len(df.columns)
        # this is good when the list of ngrams grows, but is very slow
        # old[old == ''] = np.nan
        # old = old.combine_first(df)

        # concat because we didn't pivot the dataframe; also, even with
        # the pivoted version, it's the same set of words each time
        old = pd.concat([old, df], axis=0)
        _df = old.sort_index(ascending=True)

    else:
        path.parent.mkdir(parents=True, exist_ok=True)
        _df = df

    print(f'Writing to {path}...')
    _df.to_csv(path, sep='\t', compression='gzip')
    print(f'Done.')


def collect_ousiometer_ngrams(
    pth,
    model,
    emoji_parser,
    ngrams_parser,
    savepth,
    threshold,
    target_langs,
    ousiometer_scoredir,
    case_sensitive=False,
    anchor_regex=None,
    test=False,
    process_in_ram=True,
):
    """
    Run fastText on a full day folder

    Parameters
    ----------
    pth: path to a raw compressed 15-minutes twitter file(s)
    model: path to a fastText model
    emoji_parser: a compiled regular expression that matches emojis
    ngrams_parser: a compiled regular expression that matches ngrams
    savepth: path to save predictions and ngrams
    threshold: confidence score threshold for the language identifier
    target_langs: a list of languages to parse out
    ousiometer_scoredir: path to word score files
    case_sensitive: a toggle to get case sensitive ngrams
    anchor_regex: a compiled regex to subsample tweets
    test: a  toggle for debugging mode
    process_in_ram:
    """
    pth = Path(pth)
    savedir = Path(savepth)
    if not pth.exists() or not pth.is_dir():
        print(f'Error: {pth} - no such directory')
        return False

    clf = fasttext.load_model(str(model))
    current_batches = Path(f"{savedir}/en/{pth.stem}.tsv.gz")

    if current_batches.exists():
        completed = g_utils.read_tsvgz(
            f"{savedir}/en/{pth.stem}.tsv.gz",
            header=0, index_col=0,
            # na_filter=False, sep='\t'
        )['timestamp'].unique()
    else:
        completed = None

    # -------------------------------------------------------------------------------------------------- #
    print('Classifying tweets...')
    print('-' * 60)

    if process_in_ram:
        df_dict = {lang: [] for lang in target_langs}

    # for every 15-minute chunk, sorted
    for i, b in enumerate(sorted(pth.rglob('*.gz'))):
        # check if already completed
        if completed is not None and b.stem in completed:
            continue

        print(b.stem)
        t = time.time()

        ousiometer_ngrams = process_ousiometer_batch(
            b,
            clf,
            threshold,
            emoji_parser,
            ngrams_parser,
            target_langs=target_langs,
            target_ngrams=ousiometer_scoredir,
            case_sensitive=case_sensitive,
            anchor_regex=anchor_regex,
            test=test,
        )

        for lang in ousiometer_ngrams.keys():
            df = pd.DataFrame.from_dict(ousiometer_ngrams[lang],
                                        orient='index',
                                        columns=['count', 'count_no_rt'])
            df['timestamp'] = b.stem
            df.index.name = 'word'

            print(
                f'{lang} | '
                f'MEM={round(g_utils.df_mem_usage(df) + g_utils.get_obj_size(ousiometer_ngrams[lang]), 2)} GB | '
                f'[{len(df)}] unique 1grams')

            if test:
                print(df.head())
                print(df.tail())

            if process_in_ram:
                df_dict[lang].append(df)

            else:
                update_ousiometer(
                        Path(f"{savedir}/{lang}/{pth.stem}.tsv.gz"),
                        df,
                        test=test,
                    )

            # for suffix in ['','_no_rt']:
            #     col = f'count{suffix}'
            #     if test:
            #         print(col)

            #     _df = df
            #     _df = df.reset_index().pivot(
            #                 index='timestamp', columns='word', values=col,
            #             ).rename_axis(None, axis=1)

            #     if process_in_ram:
            #         df_list.append(_df)
            #     else:
            #         update_ousiometer(
            #                 Path(f"{savedir}/{lang}/{pth.stem}.tsv.gz"),
            #                 _df,
            #                 test=test,
            #             )
        print(f'Processing time: {time.time() - t:.2f} sec.')
        print('-' * 60)

    if process_in_ram:
        print('Combining dataframes in RAM...')
        for lang in df_dict.keys():
            outputpath = Path(f"{savedir}/{lang}/{pth.stem}.tsv.gz")
            outputpath.parent.mkdir(parents=True, exist_ok=True)

            if len(df_dict[lang]) == 0:
                print(f'WARNING: No counts obtained for {lang}. Check if '
                      f'{outputpath} exists.')
                continue

            comp_df = pd.concat(df_dict[lang], axis=0)
            if outputpath.exists():
                old = g_utils.read_tsvgz(outputpath, header=0, index_col=0)
                assert old.columns.equals(comp_df.columns)
                comp_df = pd.concat([old, comp_df], axis=0)

            print(f'Saving to {outputpath}...')
            comp_df.to_csv(outputpath, sep='\t', compression='gzip')
            print('Done.')

    # -------------------------------------------------------------------------------------------------- #
    return True


def aggregate_tsv_counts(datadir, start_date, end_date,
                         timestamp_col='timestamp', outputpath=None,
                         compression='gzip'):
    """
    Aggregates the ngram counts of tsv.gz files in `datadir` with columns:
    `word`, `count`, `count_no_rt`, and `timestamp` from `start_date` to
    `end_date`, both in formats yyyy-mm-dd or yyyy-mm-dd-hh-mm.
    Note that mm can only be any of the following: 00,15,30,45.

    Parameters
    ----------
    datadir: folder where all the ngram count date folders are
    start_date: start date in yyyy-mm-dd(-hh-mm) format
    end_date: start date in yyyy-mm-dd(-hh-mm) format
    timestamp_col: name of column in data containing the timestamp
    """
    date_range = g_utils.get_daterange(start_date, end_date)
    df = g_utils.concat_tsvgz_df_daterange(datadir, date_range)

    # df = data[data['timestamp'].isin(date_range)]

    agg_df = df.groupby('word').agg({'count': 'sum', 'count_no_rt': 'sum'})
    agg_df = agg_df.reset_index()

    if outputpath is not None:
        print(f'Writing aggregated dataframe to {outputpath}...')
        agg_df.to_csv(outputpath, sep='\t', compression=compression)
        print('Done.')

    return agg_df


def merge_counts_with_scores_tsv(datadir, score_file, start_date, end_date,
                                  outputpath=None, tmp_outputpath=None):
    """
    Merges the ngram count dataframe with columns `word`, `count`,
    `count_no_rt` with the score dataframe. Temporary files are created for
    the aggregated dataframe (no scores).

    Parameters
    ----------
    datadir: folder where all the ngram count date folders are
    start_date: start date in yyyy-mm-dd(-hh-mm) format
    end_date: start date in yyyy-mm-dd(-hh-mm) format
    outputpath: file path for the final output
    """
    agg_df = aggregate_tsv_counts(datadir, start_date, end_date,
                                  timestamp_col='timestamp',
                                  outputpath=tmp_outputpath
                                  )
    score_df = g_utils.read_tsvgz(score_file, gzip=False)
    merge_df = pd.merge(score_df, agg_df, on='word', how='left').set_index(
                        'word')

    print(f'Writing files with scores in {outputpath}')
    merge_df.to_csv(outputpath, sep='\t', compression='gzip')

    return True
