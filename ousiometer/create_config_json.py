"""
Create the config.json file.
"""
from pathlib import Path
from cli import SortedMenu
import sys
import ujson


def parse_args(args):
    try:
        import cli
        parser = cli.parser()
    except Exception:
        import argparse
        parser = argparse.ArgumentParser(formatter_class=SortedMenu)

    parser.add_argument(
        "--dir",
        type=str,
        default=Path.cwd(),
        help="the installation directory; should be the path for ousiometer/ousiometer"
    )

    return parser.parse_args(args)


def main(args=None):
    if args is None:
        args = sys.argv[1:]

    args = parse_args(args)

    cwd = args.dir

    assert cwd.stem == 'ousiometer'
    assert cwd.parent.stem == 'ousiometer'

    resources = cwd.parent / 'storywrangler' / 'resources'
    ousiometer_scores = cwd.parent / 'ousiometer_scores'

    config = {
        "score_dir": str(ousiometer_scores),
        "outdir": str(cwd / 'results'),
        "model": str(resources / 'lid.176.bin'),
        "lid_threshold": 0.25,
        "langs": "en",
        "emoji_parser": str(resources / 'emojis.bin'),
        "ngrams_parser": str(resources / 'ngrams.bin'),
        "lowercase": True,
        "process_in_ram": True,
        "logdir": str(cwd / 'logs'),
        "logfile": True,
        "logscreen": True,
        "overwrite": False
    }

    json_path = resources / 'config.json'
    with open(json_path, 'w') as jsonfile:
        ujson.dump(config, jsonfile, indent=4, escape_forward_slashes=False)

    print(f'DONE: See {json_path}')


if __name__ == "__main__":
    main()
