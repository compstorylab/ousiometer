"""
Ousiometer
Copyright (c) 2021 The Computational Story Lab.
Licensed under the MIT License;
"""

import os
import sys
import logging
from datetime import datetime
from itertools import chain
from cli import SortedMenu
from pathlib import Path
import pandas as pd


def create_parser(defaults):
    """
    Creates the parser object with the log arguments included.
    """
    try:
        import cli
        parser = cli.parser()
    except Exception:
        import argparse
        parser = argparse.ArgumentParser(formatter_class=SortedMenu)

    parser.add_argument(
        "--logdir",
        type=str,
        default=defaults['logdir'],
        help="the log directory"
    )
    parser.add_argument(
        "--logfile",
        action="store_true",
        default=defaults['logfile'],
        help="create a log file"
    )
    parser.add_argument(
        "--no-logfile",
        dest="logfile",
        action="store_false",
        help="will not log on file"
    )
    parser.add_argument(
        "--logscreen",
        action="store_true",
        default=defaults['logscreen'],
        help="display the log on the screen"
    )
    parser.add_argument(
        "--no-logscreen",
        dest="logscreen",
        action="store_false",
        help="will not log on screen"
    )
    parser.add_argument(
        "--overwrite",
        action="store_true",
        default=defaults['overwrite'],
        help="overwrites the outputfile if it exists"
    )

    parser.add_argument(
        '--test',
        action='store_true',
        help='test run'
    )

    return parser


def makedir_if_needed(dir):
    if dir is not None:
        if not os.path.exists(dir):
            os.makedirs(dir)
    else:
        pass


def log_sysargs(logname, args, script=None, date=None,
                to_file=True, to_screen=False, filemode='a'):
    if date is None:
        date = datetime.now()
    if Path(logname).suffix != '.log':
        log_filename = '{0}_{1}.log'.format(logname,
                                            date.strftime('%Y%m%d-%H%M%S'))
    else:
        log_filename = logname

    if not os.path.exists(log_filename):
        filemode = 'w'

    handlers = []
    if to_file:
        fh = logging.FileHandler(log_filename, mode=filemode)  # new log everytime
        formatter = logging.Formatter(
                fmt='%(asctime)s %(message)s',
                datefmt='%m/%d/%Y %I:%M:%S %p',
            )
        fh.setFormatter(formatter)
        handlers.append(fh)

    if to_screen:
        sh = logging.StreamHandler()
        formatter = logging.Formatter(
                fmt='%(asctime)s %(message)s',
                datefmt='%m/%d/%Y %I:%M:%S %p',
            )
        sh.setFormatter(formatter)
        handlers.append(sh)

    if len(handlers) != 0:
        logging.basicConfig(
                # filename=log_filename,
                # filemode='w', # new log everytime
                # format='%(asctime)s %(message)s',
                # datefmt='%m/%d/%Y %I:%M:%S %p',
                level=logging.INFO,
                handlers=handlers,
            )

        if script is not None:
            info_text = '\n\n******SCRIPT PARAMETERS******\n\n'

            info_text += script + '\n'

            for k, v in vars(args).items():
                info_text += str(k) + '\t' + str(v) + '\n'
            info_text += '\n*****************************\n'

            logging.info(info_text)


def read_tsvgz(filepath, gzip=True, **kwargs):
    import pandas as pd
    if gzip:
        compression = 'gzip'
    else:
        compression = None
    return pd.read_csv(filepath, sep='\t', compression=compression,
                       keep_default_na=False, na_values=[''],
                       **kwargs)


def get_obj_size(obj):
    """ Compute memory footprint of an object recursively """
    dtypes = {
        tuple: iter,
        list: iter,
        dict: lambda d: chain.from_iterable(d.items()),
    }
    seen = set()

    def mem(obj):
        i = id(obj)
        if i in seen:
            return 0

        seen.add(i)
        total = sys.getsizeof(obj, sys.getsizeof(0))
        for tp, h in dtypes.items():
            if isinstance(obj, tp):
                total += sum(map(mem, h(obj)))
                break
        return total

    # convert to (GB)
    return round(mem(obj) / float(1 << 30), 2)


def df_mem_usage(d):
    """ Get memory usage for a given dataframe
    :param d: a pandas dataframe
    """
    return round(d.memory_usage(deep=True).sum() / float(1 << 30), 2)


def verify_timestamp(ts, mode):
    """
    Verifies if the timestamp is valid. If the timestamp is given only
    in yyyy-mm-dd format, the time 00-00-00 or 23-45
    is automatically appended.
    :param ts: timestamp in yyyy-mm-dd(-hh-mm) format
    :param mode: if 'start', appends '00-00-00'; if 'end', '23-45'

    :returns: valid timestamp in yyyy-mm-dd-hh-mm format
    """
    ts_split = ts.split('-')

    if len(ts_split) == 5:
        try:
            assert(ts_split[-1] in ['00', '15', '30', '45'])
            return ts
        except Exception as e:
            logging.error(e)
            raise Exception(e.message)

    elif len(ts_split) == 3:
        if mode == 'start':
            return f'{ts}-00-00'
        elif mode == 'end':
            return f'{ts}-23-45'
        else:
            msg = f'Time mode {mode} is invalid.'
            raise ValueError(msg)

    else:
        msg = f'Timestamp {ts} is invalid.'
        raise ValueError(msg)


def get_daterange(start_date, end_date, fmt='%Y-%m-%d-%H-%M',
                  offset={'minutes': 15}, **kwargs):
    """
    Gets the date range in 15-minute increments
    :param start_date: 'yyyy-mm-dd(-hh-mm)'
    :param end_date: 'yyyy-mm-dd(-hh-mm)'
    :param fmt: timestamp format to be returned
    :returns: list of dates in yyyy-mm-dd-hh-mm format
    """
    start_date = verify_timestamp(start_date, mode='start')
    end_date = verify_timestamp(end_date, mode='end')

    print(f'Computing from {start_date} to {end_date}...')

    date_range = [i.strftime(fmt) for i in pd.date_range(
        start=datetime.strptime(start_date, fmt),
        end=datetime.strptime(end_date, fmt),
        freq=pd.DateOffset(**offset))
        ]

    return date_range


def concat_tsvgz_df_daterange(datadir: str, date_range: list,
                              timestamp_col='timestamp'):
    """
    Combines the tsv.gz dataframes from `datadir` given the `date_range`
    :param datadir: directory where the daily <date>.tsv.gz files are
    :param date_range: list of strings 'yyyy-mm-dd(-hh-mm)'
    :returns: the concatenated dataframe for all timestamps in date_range
    """
    from collections import defaultdict

    dates_days_list = [('-'.join(ts.split('-')[:3]), ts) for ts in date_range]
    dates_days = defaultdict(list)
    for i, j in dates_days_list:
        dates_days[i].append(j)

    df = None
    for day, timestamps in dates_days.items():
        _data = read_tsvgz(Path(datadir) / f'{day}.tsv.gz', header=0,
                           index_col=0)
        _data = _data[_data[timestamp_col].isin(timestamps)]
        _data = _data.drop(columns={timestamp_col})
        if df is None:
            df = _data
        else:
            df = df.add(_data, fill_value=0)

    return df


def set_zero_count(df, count_cols=('count', 'count_no_rt')):
    for col in count_cols:
        df[col] = df[col].fillna(0)
    return df


def delete_words(df, delete_word_list, count_cols=('count', 'count_no_rt')):
    for col in count_cols:
        df.loc[delete_word_list, col] = 0


def add_freq_col(df, count_cols=('count', 'count_no_rt')):
    """
    Adds a frequency column in the dataframe `df` with columns `count` and
    `count_no_rt`.
    """
    for col in count_cols:
        freq_col = col.replace('count', 'freq')
        df[freq_col] = df[col] / df[col].sum()

    return df

