"""
Ousiometer
Copyright (c) 2021 The Computational Story Lab.
Licensed under the MIT License;

Aggregate n-gram counts from a start datetime to an end datetime and merges
the scores.
"""
import logging
import shutil
import sys
import time
from pathlib import Path

import utils_general as g_utils
from utils_parse import merge_counts_with_scores_tsv


def parse_args(args, config):
    parser = g_utils.create_parser(config)

    parser.add_argument(
        'datadir',
        type=str,
        help='data directory containing .tsv.gz files of the '
             'ngram counts; ex. /results/en'
    )

    parser.add_argument(
        'score',
        type=str,
        help='path for the file containing the scores for each word'
    )

    parser.add_argument(
        '--start_date',
        default=None,
        type=str,
        help='start date in yyyy-mm-dd(-hh-mm) format; note that mm is one '
             'of the following: 00,15,30,45'
    )

    parser.add_argument(
        '--end_date',
        default=None,
        type=str,
        help='start date in yyyy-mm-dd-hh-mm format; note that mm is one '
             'of the following: 00,15,30,45'
    )

    parser.add_argument(
        '--outdir',
        default=None,
        type=str,
        help='output directory for the aggregated file',
    )

    return parser.parse_args(args)


def main(args=None):
    timeit = time.time()

    if args is None:
        args = sys.argv[1:]

    config = {
        'logdir': Path.cwd() / 'logs',
        'logfile': False,
        'logscreen': True,
        'overwrite': False,
    }

    args = parse_args(args, config)

    logname = Path(config['logdir']) / f'{Path(sys.argv[0]).stem}'

    print = logging.info

    g_utils.log_sysargs(logname, args, script=sys.argv[0],
                        to_file=args.logfile,
                        to_screen=args.logscreen
                        )
    logging.info('Initialized logging...')
    logging.info(args)

    datadir = args.datadir
    score_file = args.score

    assert Path(datadir).is_dir()
    assert Path(score_file).is_file()

    start_date = args.start_date
    end_date = args.end_date
    if start_date is None or end_date is None:
        raise Exception('start_date and end_date must be specified')
    # Convert here to get a proper outputpath name
    start_date = g_utils.verify_timestamp(start_date, mode='start')
    end_date = g_utils.verify_timestamp(end_date, mode='end')

    outdir = args.outdir
    if outdir is not None:
        outputpath = str(Path(outdir) /
                         f'ngrams_agg_start={start_date}_end={end_date}.tsv.gz')
        tmp_outputpath = str(Path(outdir) / 'tmp' / Path(outputpath).stem)

        g_utils.makedir_if_needed(Path(outputpath).parent)
        g_utils.makedir_if_needed(Path(tmp_outputpath).parent)
    else:
        outputpath = None
        tmp_outputpath = None

    done = merge_counts_with_scores_tsv(datadir, score_file, start_date,
                                        end_date, outputpath=outputpath,
                                        tmp_outputpath=tmp_outputpath
                                        )

    if done:
        print('Success. Deleting unnecessary temporary files '
              f'{tmp_outputpath}... ')
        shutil.rmtree(Path(tmp_outputpath).parent, ignore_errors=True)

    print(f'Total time elapsed: {time.time() - timeit:.2f} sec. ~ {outputpath}')
    print('DONE.')


if __name__ == "__main__":
    main()
